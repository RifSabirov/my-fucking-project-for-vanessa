﻿#language: ru

@tree
@БыстрыеПроверки

Функционал: Проверка работы функциональной опции Использовать организации в форме заказа покупателя

Как Тестировщик я хочу
проверить поля  организация в форме заказа покупателя 
чтобы убедиться что пользователь не ошибется при вводе данных 

Сценарий: Проверка работы функциональной опции Использовать организации в форме заказа покупателя
	И Я устанавливаю в константу "UseCompanies" значение "False"
	Когда открылось окно 'Заказы покупателей'
	И я нажимаю на кнопку с именем 'FormCreate'
	//И элемент формы "Организация" существует и невидим на форме
	//И элемент формы "Организация" отсутствует на форме
	
 	Когда Проверяю шаги на Исключение:
		// |'И элемент формы "Организация" отсутствует на форме'|
		 |'элемент формы "Организация" существует и невидим на форме'|
 
	И элемент формы "Организация" существует и невидим на форме
	//И Я устанавливаю в константу "UseCompanies" значение "True"
	
		
